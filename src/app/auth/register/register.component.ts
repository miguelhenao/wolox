import { AuthService } from './../../shared/services/auth.service';
import { SignUp } from './../../shared/models/SignUp.model';
import { ApiService } from './../../shared/services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [ApiService, AuthService]
})
export class RegisterComponent implements OnInit {
  countries: Array<any>; // Lista de paises

  // Toast de notificación
  private Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });

  private data: SignUp = {
    name: '',
    last_name: '',
    country: '',
    province: '',
    mail: '',
    phone: '',
    password: ''
  }; // Objeto SignUp

  constructor(private fb: FormBuilder, private serviceApi: ApiService, private serviceAuth: AuthService, private route: Router) { }

  /**
   * FormGroup para formulario de registro
   */
  formSignUp = this.fb.group({
    name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(30)])),
    last_name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(30)])),
    country: new FormControl('', Validators.compose([Validators.required])),
    province: new FormControl('', Validators.compose([Validators.required])),
    mail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    phone: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
    password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6),
    Validators.pattern(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{6,14}$/)])),
    confirmPassword: new FormControl('', Validators.required),
    terms: new FormControl(false, Validators.compose([Validators.requiredTrue]))
  });

  ngOnInit(): void {
    this.getCountries();
  }

  /**
   * Obtiene la lista de Paises con sus provincias
   */
  private getCountries(): void {
    this.countries = this.serviceApi.listOfCountries();
  }

  /**
   * Obtiene la lista de Provincias escogido un país
   */

  getProvinces(): Array<any> {
    if (this.formSignUp.get('country').invalid) {
      return [];
    }
    return this.countries.find(x => x.country === this.formSignUp.get('country').value).provinces;
  }

  /**
   * Verifica si el control no cumple con alguno de los validadores
   * @param controlName --> Nombre del control
   * @param validationType --> Tipo de validación
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.formSignUp.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  /**
   * Verifica si la contraseña es válida, confirmar contraseña
   * @returns -->
   */
  isValidPassword(): boolean {
    return ((this.formSignUp.get('password').value) === this.formSignUp.get('confirmPassword').value) ? true : false;
  }

  /**
   * Método submit para recoger los datos del formulario y enviarlos al servicio
   */
  onSubmit(): void {
    if (this.formSignUp.valid) {
      this.data.name = this.formSignUp.value.name;
      this.data.last_name = this.formSignUp.value.last_name;
      this.data.country = this.formSignUp.value.country;
      this.data.province = this.formSignUp.value.province;
      this.data.mail = this.formSignUp.value.mail;
      this.data.phone = this.formSignUp.value.phone;
      this.data.password = this.formSignUp.value.password;
      this.serviceAuth.signUp(this.data).subscribe(success => {
        console.log(success);
        this.Toast.fire({
          icon: 'success',
          title: 'Registro satisfactorio'
        });
        localStorage.setItem('user', this.data.mail);
        localStorage.setItem('password', this.data.password);
        localStorage.setItem('token', success.token);
        this.route.navigate(['/list']);
      }, error => {
        console.error(error);
        this.Toast.fire({
          icon: 'error',
          title: 'Se ha producido un error!'
        });
      });
    }
  }

}
