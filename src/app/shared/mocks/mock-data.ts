import { Technology } from './../models/Technology.model';

export const TECHS: Technology[] = [
  {
    tech: 'Node',
    year: '2009',
    author: 'Ryan Dahl',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
  },
  {
    tech: 'React',
    year: '2013',
    author: 'Jordan Walke',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
  },
  {
    tech: 'Vue',
    year: '2014',
    author: 'Evan You',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/220px-Vue.js_Logo_2.svg.png'
  },
  {
    tech: 'Ruby on Rails',
    year: '2005',
    author: 'David Heinemeier Hansson',
    license: 'MIT',
    language: 'Ruby',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/220px-Ruby_On_Rails_Logo.svg.png'
  },
  {
    tech: 'iOS',
    year: '2007',
    author: 'Apple Inc.',
    license: 'Proprietary',
    language: 'Swift, Objective-C',
    type: 'Mobile',
    logo: 'http://pluspng.com/img-png/apple-ios-logo-png-ios-apple-logo-vector-download-280.png'
  },
  {
    tech: 'Android',
    year: '2008',
    author: 'Google',
    license: 'Apache',
    language: 'Java, Kotlin',
    type: 'Mobile',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Android_robot_2014.svg/75px-Android_robot_2014.svg.png'
  },
  {
    tech: 'Angular',
    year: '2016',
    author: 'Google',
    license: 'MIT',
    language: 'TypeScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png'
  },
  {
    tech: 'Springboot',
    year: '2012',
    author: 'Pivotal Software',
    license: 'Apache 2.0',
    language: 'Java',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/220px-Spring_Framework_Logo_2018.svg.png'
  }
];

export const TECHS_FOR_FILTER: Technology[] = [
  {
    tech: 'Node',
    year: '2009',
    author: 'Ryan Dahl',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
  },
  {
    tech: 'React',
    year: '2013',
    author: 'Jordan Walke',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
  },
  {
    tech: 'Vue',
    year: '2014',
    author: 'Evan You',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/220px-Vue.js_Logo_2.svg.png'
  },
  {
    tech: 'Ruby on Rails',
    year: '2005',
    author: 'David Heinemeier Hansson',
    license: 'MIT',
    language: 'Ruby',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/220px-Ruby_On_Rails_Logo.svg.png'
  },
  {
    tech: 'iOS',
    year: '2007',
    author: 'Apple Inc.',
    license: 'Proprietary',
    language: 'Swift, Objective-C',
    type: 'Mobile',
    logo: 'http://pluspng.com/img-png/apple-ios-logo-png-ios-apple-logo-vector-download-280.png'
  },
  {
    tech: 'Android',
    year: '2008',
    author: 'Google',
    license: 'Apache',
    language: 'Java, Kotlin',
    type: 'Mobile',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Android_robot_2014.svg/75px-Android_robot_2014.svg.png'
  },
  {
    tech: 'Angular',
    year: '2016',
    author: 'Google',
    license: 'MIT',
    language: 'TypeScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png'
  },
  {
    tech: 'Springboot',
    year: '2012',
    author: 'Pivotal Software',
    license: 'Apache 2.0',
    language: 'Java',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/220px-Spring_Framework_Logo_2018.svg.png'
  }
];

export const TECHS_ORDER_ASC: Technology[] = TECHS.sort((a, b) => (a.tech > b.tech) ? 1 : (a.tech < b.tech) ? -1 : 0);

export const TECHS_ORDER_DESC: Technology[] = TECHS.sort((a, b) => (a.tech > b.tech) ? -1 : (a.tech < b.tech) ? 1 : 0);

export const TECHS_FILTER_A: Technology[] = [
  {
    tech: 'Angular',
    year: '2016',
    author: 'Google',
    license: 'MIT',
    language: 'TypeScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png'
  },
  {
    tech: 'Android',
    year: '2008',
    author: 'Google',
    license: 'Apache',
    language: 'Java, Kotlin',
    type: 'Mobile',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Android_robot_2014.svg/75px-Android_robot_2014.svg.png'
  }
];

export const TECHS_FILTER_A_ORDER_ASC: Technology[] = [
  {
    tech: 'Android',
    year: '2008',
    author: 'Google',
    license: 'Apache',
    language: 'Java, Kotlin',
    type: 'Mobile',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Android_robot_2014.svg/75px-Android_robot_2014.svg.png'
  },
  {
    tech: 'Angular',
    year: '2016',
    author: 'Google',
    license: 'MIT',
    language: 'TypeScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png'
  }
];

export const TECHS_FILTER_R: Technology[] = [
  {
    tech: 'React',
    year: '2013',
    author: 'Jordan Walke',
    license: 'MIT',
    language: 'JavaScript',
    type: 'Front-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
  },
  {
    tech: 'Ruby on Rails',
    year: '2005',
    author: 'David Heinemeier Hansson',
    license: 'MIT',
    language: 'Ruby',
    type: 'Back-End',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/220px-Ruby_On_Rails_Logo.svg.png'
  }
];
