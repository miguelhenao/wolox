import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Technology } from '../models/Technology.model';
import * as countries from '../../../assets/countries.json';

const API_URL = environment.gateway;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  headers = new HttpHeaders()
    .set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  listAllTechs(): Observable<Array<Technology>> {
    return this.http.get<Array<Technology>>(API_URL + '/techs');
  }

  listOfCountries(): Array<any> {
    return countries['default'];
  }
}
