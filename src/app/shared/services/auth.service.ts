import { Token } from './../models/Token.model';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login } from '../models/Login.model';
import { Observable } from 'rxjs';
import { SignUp } from '../models/SignUp.model';

const API_URL = environment.gateway;


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new HttpHeaders()
    .set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  login(user: Login): Observable<Token> {
    return this.http.post<Token>(API_URL + '/login', user, { headers: this.headers });
  }

  signUp(user: SignUp): Observable<Token> {
    return this.http.post<Token>(API_URL + '/signup', user, { headers: this.headers });
  }
}
